/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package hospital;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class ResearchGroup {
    private Patients patient;

    /**
     * Get the value of patient
     *
     * @return the value of patient
     */
    public Patients getPatient() {
        return patient;
    }

    /**
     * Set the value of patient
     *
     * @param patient new value of patient
     */
    public void setPatient(Patients patient) {
        this.patient = patient;
    }
}

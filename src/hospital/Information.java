/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package hospital;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Information {
     private String specificMedicalDetails;

    /**
     * Get the value of specificMedicalDetails
     *
     * @return the value of specificMedicalDetails
     */
    public String getSpecificMedicalDetails() {
        return specificMedicalDetails;
    }

    /**
     * Set the value of specificMedicalDetails
     *
     * @param specificMedicalDetails new value of specificMedicalDetails
     */
    public void setSpecificMedicalDetails(String specificMedicalDetails) {
        this.specificMedicalDetails = specificMedicalDetails;
    }
}

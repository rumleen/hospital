/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package hospital;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class WaitTimeGroup extends ResearchGroup {
    private int numberOfPatients;
    private int waitingTime;

    /**
     * Get the value of waitingTime
     *
     * @return the value of waitingTime
     */
    public int getWaitingTime() {
        return waitingTime;
    }

    /**
     * Set the value of waitingTime
     *
     * @param waitingTime new value of waitingTime
     */
    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }


    /**
     * Get the value of numberOfPatients
     *
     * @return the value of numberOfPatients
     */
    public int getNumberOfPatients() {
        return numberOfPatients;
    }

    /**
     * Set the value of numberOfPatients
     *
     * @param numberOfPatients new value of numberOfPatients
     */
    public void setNumberOfPatients(int numberOfPatients) {
        if (numberOfPatients >= 10) {
            this.numberOfPatients = numberOfPatients;
        } else {
            System.out.println("Number Of Patients must be 10 or more");
        }
    }
}

/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package hospital;
import java.util.ArrayList;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Patients {
    private ArrayList<WristBand> wristBands;    
    private String patientsName;    
    private String dateOfBirth;
    private Doctor familyDoctor;

    /**
     * Get the value of familyDoctor
     *
     * @return the value of familyDoctor
     */
    public Doctor getFamilyDoctor() {
        return familyDoctor;
    }

    /**
     * Set the value of familyDoctor
     *
     * @param familyDoctor new value of familyDoctor
     */
    public void setFamilyDoctor(Doctor familyDoctor) {
        this.familyDoctor = familyDoctor;
    }


    /**
     * Get the value of dateOfBirth
     *
     * @return the value of dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Set the value of dateOfBirth
     *
     * @param dateOfBirth new value of dateOfBirth
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


    /**
     * Get the value of patientsName
     *
     * @return the value of patientsName
     */
    public String getPatientsName() {
        return patientsName;
    }

    /**
     * Set the value of patientsName
     *
     * @param patientsName new value of patientsName
     */
    public void setPatientsName(String patientsName) {
        this.patientsName = patientsName;
    }


    /**
     * Get the value of wristBands
     *
     * @return the value of wristBands
     */
    public ArrayList<WristBand> getWristBands() {
        return wristBands;
    }

    /**
     * Set the value of wristBands
     *
     * @param wristBands new value of wristBands
     */
    public void setWristBands(ArrayList<WristBand> wristBands) {
        this.wristBands = wristBands;
    }
}

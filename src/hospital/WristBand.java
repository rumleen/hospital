/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package hospital;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class WristBand {
    private BarCode barCode;
    private Information information;

    /**
     * Get the value of information
     *
     * @return the value of information
     */
    public Information getInformation() {
        return information;
    }

    /**
     * Set the value of information
     *
     * @param information new value of information
     */
    public void setInformation(Information information) {
        this.information = information;
    }

    /**
     * Get the value of barCode
     *
     * @return the value of barCode
     */
    public BarCode getBarCode() {
        return barCode;
    }

    /**
     * Set the value of barCode
     *
     * @param barCode new value of barCode
     */
    public void setBarCode(BarCode barCode) {
        this.barCode = barCode;
    }
}

/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package hospital;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class BarCode {
    private String uniqueBarCode;

    /**
     * Get the value of uniqueBarCode
     *
     * @return the value of uniqueBarCode
     */
    public String getUniqueBarCode() {
        return uniqueBarCode;
    }

    /**
     * Set the value of uniqueBarCode
     *
     * @param uniqueBarCode new value of uniqueBarCode
     */
    public void setUniqueBarCode(String uniqueBarCode) {
        this.uniqueBarCode = uniqueBarCode;
    }
}
